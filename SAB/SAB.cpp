#include <bits/stdc++.h>

#include <algorithm>

#include <iostream>

#include <iterator>

#include <numeric>

#include <sstream>

#include <fstream>

#include <cassert>

#include <climits>

#include <cstdlib>

#include <cstring>

#include <string>

#include <cstdio>

#include <vector>

#include <cmath>

#include <queue>

#include <deque>

#include <stack>

#include <list>

#include <map>

#include <unordered_map>

#include <set>

#include <unordered_set>

#include <chrono>


using namespace std;

using namespace chrono;

typedef std::tuple < string, string, int > edge_data;

class DisjSet {
  int * rank, * parent, n;

  public:
    int inita(int n) {
      rank = new(nothrow) int[n];

      parent = new(nothrow) int[n];

      if (rank == nullptr || parent == nullptr) {
        cout << "!!!!empty" << endl;
        return 1;
      }
      this -> n = n;
      makeSet();
      return 0;
    }

    ~DisjSet() {
      if (rank != nullptr) {
        delete[] rank;
      }
      if (parent != nullptr) {
        delete[] parent;
      }
    }

  void makeSet() {
    for (int i = 0; i < n; i++) {
      parent[i] = i;
    }
  }

  int find(int x) {

    if (parent[x] != x) {
      parent[x] = find(parent[x]);

    }

    return parent[x];
  }

  void Union(int x, int y) {

    int xset = find(x);
    int yset = find(y);

    if (xset == yset)
      return;

    if (rank[xset] < rank[yset]) {
      parent[xset] = yset;
    } else if (rank[xset] > rank[yset]) {
      parent[yset] = xset;
    } else {
      parent[yset] = xset;
      rank[xset] = rank[xset] + 1;
    }
  }

};

void readGraph(vector < edge_data > & multiplicity, std::unordered_set < std::string > & vertexSet,
  char * filename, size_t & numberEdges, int & kmer) {
  ifstream file(filename);

  string data;

  while (getline(file, data)) {
    string v1, v2;
    int multi;

    istringstream buffer(data);
    buffer >> v1 >> v2 >> multi;
    if (kmer == 0) {
      kmer = v1.length();
    }
    vertexSet.insert(v1);
    vertexSet.insert(v2);
    if (v1.compare(v2) != 0) {
      multiplicity.push_back(make_tuple(v1, v2, multi));
      numberEdges += multi;
    }
    buffer.clear();
  }
  file.close();
}

void countDegree(vector < edge_data > & multiplicity,
  std::unordered_map < string, int > & vertexMap, int * degreeNode) {

  std::unordered_map < string, int > ::iterator iter;
  std::unordered_map < string, int > ::iterator iter2;

  for (int i = 0; i < multiplicity.size(); i++) {
    string str1 = get < 0 > (multiplicity[i]);
    string str2 = get < 1 > (multiplicity[i]);
    int multi = get < 2 > (multiplicity[i]);

    int node1 = -1;
    int node2 = -1;
    iter = vertexMap.find(str1);
    if (iter != vertexMap.end()) {
      node1 = iter -> second;
    }
    iter2 = vertexMap.find(str2);
    if (iter2 != vertexMap.end()) {
      node2 = iter2 -> second;
    }

    degreeNode[node1] = degreeNode[node1] - multi;
    degreeNode[node2] = degreeNode[node2] + multi;
  }

}

void createUnion(vector < edge_data > & multiplicity, int numberNode, std::unordered_map < string, int > & vertexMap,
  DisjSet & unionSet) {
  for (int i = 0; i < multiplicity.size(); i++) {
    string str1 = get < 0 > (multiplicity[i]);
    string str2 = get < 1 > (multiplicity[i]);
    std::unordered_map < string, int > ::iterator iter1;
    std::unordered_map < string, int > ::iterator iter2;
    iter1 = vertexMap.find(str1);
    iter2 = vertexMap.find(str2);
    if (iter1 != vertexMap.end() && iter2 != vertexMap.end()) {
      unionSet.Union((int) iter1 -> second, (int) iter2 -> second);
    }

  }

}

int buildMatchingMachine(const vector < string > & words, size_t & totalStates,
  vector < std::unordered_set < int >> & L, std::unordered_set < int > & terminal,
  vector < vector < int >> & stateLevel, std::unordered_map < string, int > & vertexMap,
  int * degreeNode, vector < int > & f, int kmer) {

  std::vector < int > v(4, -1);

  std::vector < std::vector < int >> g(words.size() * kmer, v);

  int numWords = words.size();
  int states = 1;

  for (int i = 0; i < words.size(); ++i) {
    const string & keyword = words[i];

    std::unordered_map < string, int > ::iterator nodeID_iter;
    nodeID_iter = vertexMap.find(keyword);
    int currentNodeID = nodeID_iter -> second;

    L[0].insert(currentNodeID);

    vector < int > statesForCurtNode;
    int currentState = 0;
    for (int j = 0; j < keyword.size(); ++j) {

      int c = -1;
      if (keyword[j] == 'A') {
        c = 0;
      } else if (keyword[j] == 'C') {
        c = 1;
      } else if (keyword[j] == 'G') {
        c = 2;
      } else if (keyword[j] == 'T') {
        c = 3;
      }

      if (g[currentState][c] == -1) {
        g[currentState][c] = states++;

      }
      currentState = g[currentState][c];

      L[currentState].insert(currentNodeID);

      statesForCurtNode.push_back(currentState);
    }

  }

  totalStates = states;

  L.resize(totalStates);

  for (int c = 0; c < 4; ++c) {
    if (g[0][c] == -1) {
      g[0][c] = 0;
    }
  }

  vector < int > temp;
  temp.push_back(0);
  stateLevel.push_back(temp);
  temp.clear();

  queue < int > q;
  for (int c = 0; c < 4; ++c) {
    if (g[0][c] != -1 and g[0][c] != 0) {
      f[g[0][c]] = 0;
      q.push(g[0][c]);
      temp.push_back(g[0][c]);
    }
  }

  stateLevel.push_back(temp);

  int index = 1;
  int lastState = stateLevel[1][q.size() - 1];
  temp.clear();
  while (q.size()) {
    int state = q.front();
    q.pop();
    for (int c = 0; c < 4; ++c) {
      if (g[state][c] != -1) {
        int failure = f[state];
        while (g[failure][c] == -1) {
          failure = f[failure];
        }
        failure = g[failure][c];
        f[g[state][c]] = failure;
        q.push(g[state][c]);
        temp.push_back(g[state][c]);
      }
    }
    if (state == lastState && temp.size() != 0) {
      index++;
      stateLevel.push_back(temp);
      lastState = stateLevel[index][temp.size() - 1];
      temp.clear();
    }
  }
  for (auto & it: stateLevel[stateLevel.size() - 1]) {
    terminal.insert(it);
  }
  stateLevel.pop_back();

  return states;
}

void findimbalanceNode(int * degreeNode, std::unordered_multimap < int, int > & feasibleEdge,
  int & sum, vector < int > & f, vector < string > & I, std::unordered_map < string, int > vertexMap,
  vector < std::unordered_set < int >> & L, std::unordered_set < int > & terminal) {

  std::unordered_map < string, int > ::iterator iter;
  for (auto & it: I) {
    iter = vertexMap.find(it);
    if (iter != vertexMap.end()) {
      int nodeid = iter -> second;
      if (degreeNode[nodeid] < 0) {
        sum = sum - degreeNode[nodeid];
      } else if (degreeNode[nodeid] > 0) {
        sum = sum + degreeNode[nodeid];
      }

    }

  }
  for (auto & it: terminal) {
    int terminalNodeID = 0;
    auto in_iter = L[it].begin();
    terminalNodeID = * in_iter;

    if (degreeNode[terminalNodeID] > 0) {
      feasibleEdge.insert({
        f[it],
        it
      });
    }
  }

}

void balanceNode(vector < std::unordered_set < int >> & L, std::unordered_multimap < int, int > & feasibleEdge,
  int & numExtraEdge, int * degreeNode, int s, size_t & cost, int weight, int sum, vector < int > & f) {

  std::unordered_multimap < int, int > ::const_iterator Values = feasibleEdge.find(s);
  int Number = feasibleEdge.count(s);
  for (int i = 0; i < Number; i++) {
    int terminalState = Values -> second;

    int terminalNodeID = 0;
    auto in_iter = L[terminalState].begin();
    terminalNodeID = * in_iter;
    while (degreeNode[terminalNodeID] != 0 && L[s].size() != 0) {

      auto iter_u = L[s].begin();
      int u = * iter_u;
      if (degreeNode[u] < 0 && terminalNodeID != u) {
        int minOfTwoNode = min(abs(degreeNode[terminalNodeID]), abs(degreeNode[u]));

        int minNumber = min(minOfTwoNode, sum - 1 - numExtraEdge);

        numExtraEdge += minNumber;

        cost += weight * minNumber;
        degreeNode[terminalNodeID] = degreeNode[terminalNodeID] - minNumber;
        degreeNode[u] = degreeNode[u] + minNumber;
      }
      if (degreeNode[u] >= 0) {
        L[s].erase(u);
      }
      if (numExtraEdge == sum - 1) {
        return;
      }
    }
    ++Values;
    if (degreeNode[terminalNodeID] != 0) {
      feasibleEdge.insert({
        f[s],
        terminalState
      });
    }
  }

}

void findEulerianTrail(int * degreeNode, std::unordered_set < int > & terminal, vector < std::unordered_set < int >> & L,
  int numVertex, vector < vector < int >> & stateLevel, size_t & cost, vector < int > & f, vector < string > & I,
  std::unordered_map < string, int > & vertexMap, int kmer) {

  int sum = 0;
  std::unordered_multimap < int, int > feasibleEdge;
  findimbalanceNode(degreeNode, feasibleEdge, sum, f, I, vertexMap, L, terminal);

  sum = sum / 2;
  if (sum == 1)
    return;
  int sizeOfLevel = stateLevel.size();
  int numExtraEdge = 0;

  for (int i = sizeOfLevel - 1; i >= 0; i--) {
    for (auto & s: stateLevel[i]) {
      balanceNode(L, feasibleEdge, numExtraEdge, degreeNode, s, cost, kmer - i, sum, f);
      feasibleEdge.erase(s);

      if (numExtraEdge == sum - 1) {
        return;
      }
    }
  }

}

void cleanL(vector < std::unordered_set < int >> & L, int * degreeNode, std::unordered_set < int > & terminal) {
  int sizeL = L.size();
  std::unordered_set < int > ::iterator set_iter;

  for (int i = 0; i < sizeL; i++) {
    set_iter = terminal.find(i);
    if (set_iter == terminal.end()) {
      for (auto it = L[i].begin(); it != L[i].end();) {
        if (degreeNode[ * it] >= 0) {
          L[i].erase(it++);
        } else {
          ++it;
        }
      }
    }

  }
}

string readFileIntoString(const string & path) {
  ifstream input_file(path);
  if (!input_file.is_open()) {
    cerr << "Could not open the file - '" <<
      path << "'" << endl;
    exit(EXIT_FAILURE);
  }
  return string((std::istreambuf_iterator < char > (input_file)), std::istreambuf_iterator < char > ());
}


void algo2v2(string file, std::unordered_map < string, int > & vertexMap, DisjSet & unionSet, size_t & cost, int * degreeNode,
  int kmer, std::unordered_set < std::string > vertexSet, string filename) {

 // string filename(file + ".superstring");
  string SCS;

  SCS = readFileIntoString(filename);

  string u = SCS.substr(0, kmer);
  string v;
  int weight;
  int flag_u = 0;

  vector < vector < pair < int, int >>> sortEdge(kmer + 1);

  for (int i = 1; i < SCS.length() - (kmer - 1); i++) {
    v = SCS.substr(i, kmer);
    auto iter = vertexSet.find(v);
    if (iter != vertexSet.end()) {
      auto node1_iter = vertexMap.find(u);
      auto node2_iter = vertexMap.find(v);
      int node1 = -1;
      int node2 = -1;

      if (node1_iter != vertexMap.end() && node2_iter != vertexMap.end()) {
        node1 = node1_iter -> second;
        node2 = node2_iter -> second;
      }

      if (unionSet.find(node1) != unionSet.find(node2)) {
        int weight = i - flag_u;
        sortEdge[weight].push_back({
          node1,
          node2
        });
      }
      u = v;
      flag_u = i;
    }

  }

  for (int i = 1; i <= kmer; i++) {
    for (auto & edge: sortEdge[i]) {
      int node1 = edge.first;
      int node2 = edge.second;

      if (unionSet.find(node1) != unionSet.find(node2)) {
        degreeNode[node1]--;
        degreeNode[node2]++;
        unionSet.Union(node1, node2);
        cost += i;

      }
    }

  }

}

void findISet(vector < string > & I, int * degreeNode, int numVertex, std::unordered_set < std::string > vertexSet,
  std::unordered_map < string, int > & vertexMap) {
  for (auto & it: vertexSet) {
    auto iter = vertexMap.find(it);
    int node = -1;
    if (iter != vertexMap.end()) {
      node = iter -> second;
      if (degreeNode[node] != 0) {
        I.push_back(it);
      }
    }

  }

}

int main(int argc, char * argv[]) {
  vector < edge_data > multiplicity;
  std::unordered_set < std::string > vertexSet;
  size_t numberEdges = 0;
  int kmer = 0;

  readGraph(multiplicity, vertexSet, argv[1], numberEdges, kmer);

  int numVertex = vertexSet.size();

  cout << "Size of Distinct Edges = " << multiplicity.size() << endl;
  cout << "Total Edges = " << numberEdges << endl;

  std::unordered_map < string, int > vertexMap;
  int index = 0;
  for (auto & it: vertexSet) {
    vertexMap.insert(make_pair(it, index));

    index++;
  }

  DisjSet unionSet;
  if (unionSet.inita(numVertex)) {
    cout << "!!!!!!!!No memory" << endl;
    return 0;
  }
  createUnion(multiplicity, numVertex, vertexMap, unionSet);

  int * degreeNode = new int[numVertex];
  for (int i = 0; i < numVertex; i++) {
    degreeNode[i] = 0;
  }

  countDegree(multiplicity, vertexMap, degreeNode);

  size_t cost = 0;

  auto start_ph1 = high_resolution_clock::now();

  algo2v2((string) argv[1], vertexMap, unionSet, cost, degreeNode, kmer, vertexSet, (string)argv[2]);

  auto end_ph1 = high_resolution_clock::now();
  auto duration_ph1 = duration_cast < milliseconds > (end_ph1 - start_ph1);
  double time_phase1 = duration_ph1.count();
  cout << "Runtime for create find edge to connect component : " << time_phase1 << " milliseconds" << endl;
  cout << "Cost for connect component = " << cost << endl;

  vector < string > I;
  findISet(I, degreeNode, numVertex, vertexSet, vertexMap);
  size_t totalStates = 0;
  vector < std::unordered_set < int >> L;
  L.resize(I.size() * kmer);
  cout << "#. Imbalanced Nodes  = " << I.size() << endl;

  std::unordered_set < int > terminal;
  vector < vector < int >> stateLevel;
  vector < int > f(I.size() * kmer, -1);

  auto start_ac = high_resolution_clock::now();

  buildMatchingMachine(I, totalStates, L, terminal, stateLevel, vertexMap, degreeNode, f, kmer);

  f.resize(totalStates);
  L.resize(totalStates);

  auto end_ac = high_resolution_clock::now();
  auto duration_ac = duration_cast < milliseconds > (end_ac - start_ac);
  double time_ac = duration_ac.count();
  cout << "Runtime for create AC Machine : " << time_ac << " milliseconds" << endl;

  size_t cost_rda = cost;

  cleanL(L, degreeNode, terminal);

  auto start_phase2 = high_resolution_clock::now();
  findEulerianTrail(degreeNode, terminal, L, numVertex, stateLevel, cost, f, I, vertexMap, kmer);

  auto end_phase2 = high_resolution_clock::now();
  auto duration_phase2 = duration_cast < milliseconds > (end_phase2 - start_phase2);
  double time_phase2 = duration_phase2.count();
  cout << "Runtime Phase 2: " << time_phase2 << " milliseconds" << endl;

  delete[] degreeNode;

  cout << "Extension Cost  = " << cost << endl;

  return 0;
}