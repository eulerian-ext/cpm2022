#include <bits/stdc++.h>

#include <algorithm>

#include <iostream>

#include <iterator>

#include <numeric>

#include <sstream>

#include <fstream>

#include <cassert>

#include <climits>

#include <cstdlib>

#include <cstring>

#include <string>

#include <cstdio>

#include <vector>

#include <cmath>

#include <queue>

#include <deque>

#include <stack>

#include <list>

#include <map>

#include <unordered_map>

#include <set>

#include <unordered_set>

#include <chrono>

#include <boost/config.hpp>

#include <boost/graph/adjacency_list.hpp>

#include <boost/graph/connected_components.hpp>


using namespace std;
using namespace boost;
using namespace chrono;

typedef property < vertex_name_t, std::string > VertexProperty;
typedef adjacency_list < vecS, vecS, undirectedS, VertexProperty > vector_graph_t;

typedef std::tuple < string, string, int > edge_data;

class StateClass {
  public:
    std::unordered_map < int, std::unordered_set < int >> HC;

  std::unordered_map < int, std::unordered_set < int >> HE;

  public:

    bool findHC(int color) {
      std::unordered_map < int, std::unordered_set < int >> ::iterator iter;
      iter = HC.find(color);
      if (iter != HC.end()) {
        return true;
      } else {
        return false;
      }
      return false;
    }

  bool findHE(int color) {
    std::unordered_map < int, std::unordered_set < int >> ::iterator iter;
    iter = HE.find(color);
    if (iter != HE.end()) {
      return true;
    } else {
      return false;
    }

    return false;
  }

  void insertHC(int color, int state) {
    if (findHC(color)) {
      HC[color].insert(state);
    } else {
      std::unordered_set < int > temp;
      temp.insert(state);
      HC.insert({
        color,
        temp
      });
    }
  }

  void insertHE(int color, int state) {
    if (findHE(color)) {
      HE[color].insert(state);
    } else {
      std::unordered_set < int > temp;
      temp.insert(state);
      HE.insert({
        color,
        temp
      });
    }

  }

  bool eraseColorHC(int color) {
    HC.erase(color);
    if (HC.size() == 0) {
      return true;
    }

    return false;

  }

  bool eraseColorHE(int color) {
    HE.erase(color);
    if (HE.size() == 0) {
      return true;
    }

    return false;

  }
  bool deleteElemntHC(int color, int state) {
    if (findHC(color)) {
      HC[color].erase(state);
    }
    if (HC[color].size() == 0) {
      if (eraseColorHC(color)) {
        return true;
      }
    }
    return false;
  }

  bool deleteElemntHE(int color, int state) {
    if (findHE(color)) {
      HE[color].erase(state);
    }
    if (HE[color].size() == 0) {
      if (eraseColorHE(color)) {
        return true;
      }
    }
    return false;
  }

  void updateColorHC(int a, int b) {
    if (findHC(a)) {
      if (findHC(b)) {
        for (auto & it: HC[a]) {
          HC[b].insert(it);
        }
      } else {
        HC.insert({
          b,
          HC[a]
        });
      }
      HC.erase(a);
    }
  }

  void updateColorHE(int a, int b) {
    if (findHE(a)) {

      if (findHE(b)) {
        for (auto & it: HE[a]) {
          HE[b].insert(it);
        }
      } else {
        HE.insert({
          b,
          HE[a]
        });
      }

      HE.erase(a);

    }
  }

};

void readGraph(vector < edge_data > & multiplicity, std::unordered_set < std::string > & vertexSet,
  char * filename, size_t & numberEdges, int & kmer) {
  ifstream file(filename);

  string data;

  while (getline(file, data)) {
    string v1, v2;
    int multi;

    istringstream buffer(data);
    buffer >> v1 >> v2 >> multi;
    if (kmer == 0) {
      kmer = v1.length();
    }
    vertexSet.insert(v1);
    vertexSet.insert(v2);
    if (v1.compare(v2) != 0) {

      multiplicity.push_back(make_tuple(v1, v2, multi));

      numberEdges += multi;
    }
    buffer.clear();
  }
  file.close();
}

void findComponent(vector < edge_data > & multiplicity,
  std::unordered_set < std::string > vertexSet, size_t & num,
  vector < vector < string >> & comp) {

  const size_t numVertex = vertexSet.size();

  map < std::string, vector_graph_t::vertex_descriptor > indexes;

  vector_graph_t g(numVertex);

  size_t index = 0;
  for (auto & it: vertexSet) {
    boost::put(vertex_name_t(), g, index, it.c_str());
    indexes[it.c_str()] = boost::vertex(index, g);
    index++;
  }

  for (int i = 0; i < multiplicity.size(); i++) {
    string node1 = get < 0 > (multiplicity[i]);
    string node2 = get < 1 > (multiplicity[i]);
    boost::add_edge(indexes[node1], indexes[node2], g);

  }

  vector < size_t > component(num_vertices(g));
  num = connected_components(g, & component[0]);
  cout << "Number of vertices: " << boost::num_vertices(g) << '\n';

  cout << "Total number of components: " << num << endl;

  vector < string > result(num);

  comp.resize(num);
  if (num > 1) {
    index = 0;
    for (auto & it: vertexSet) {

      comp[component[index]].push_back(it);
      index++;
    }
  }

}

void graphStatisticsNew(std::unordered_map < string, int > & vertexMap,
  vector < vector < string >> & comp, vector < string > & I, int * degreeNode,
  std::unordered_set < int > & isolate_comp, std::unordered_map < int, int > & colorMap) {

  std::unordered_map < string, int > ::iterator iter;
  size_t numVertex = vertexMap.size();

  int colorID = 0;

  for (auto & it: comp) {
    size_t flag = 0;
    for (auto & it2: it) {
      iter = vertexMap.find(it2);
      size_t currentID = 0;
      if (iter != vertexMap.end()) {
        currentID = iter -> second;
      }

      if (degreeNode[currentID] != 0) {
        flag = 1;

        I.push_back(it2);
        colorMap.insert({
          currentID,
          colorID
        });
      }
    }

    if (flag == 0) {
      for (auto & it2: it) {
        iter = vertexMap.find(it2);
        size_t currentID = 0;
        if (iter != vertexMap.end()) {
          currentID = iter -> second;
        }
        isolate_comp.insert(currentID);
        I.push_back(it2);
        colorMap.insert({
          currentID,
          colorID
        });
      }
    }
    colorID++;
  }
}

void graphStatistics1compNew(std::unordered_map < string, int > & vertexMap,
  vector < string > & I, int * degreeNode) {

  for (auto & it: vertexMap) {
    if (degreeNode[it.second] != 0) {
      I.push_back(it.first);
    }
  }
}

void countDegree(vector < edge_data > & multiplicity,
  std::unordered_map < string, int > & vertexMap, int * degreeNode) {
  std::unordered_map < string, int > ::iterator iter;
  std::unordered_map < string, int > ::iterator iter2;

  for (int i = 0; i < multiplicity.size(); i++) {
    string str1 = get < 0 > (multiplicity[i]);
    string str2 = get < 1 > (multiplicity[i]);
    int multi = get < 2 > (multiplicity[i]);

    int node1 = -1;
    int node2 = -1;
    iter = vertexMap.find(str1);
    if (iter != vertexMap.end()) {
      node1 = iter -> second;
    }
    iter2 = vertexMap.find(str2);
    if (iter2 != vertexMap.end()) {
      node2 = iter2 -> second;
    }

    degreeNode[node1] = degreeNode[node1] - multi;
    degreeNode[node2] = degreeNode[node2] + multi;
  }

}

int buildMatchingMachine(const vector < string > & words, size_t & totalStates,
  vector < std::unordered_set < int >> & L, std::unordered_set < int > & terminal,
  vector < vector < int >> & stateLevel, std::unordered_map < string, int > & vertexMap,
  int * degreeNode, vector < int > & f, std::unordered_map < int, int > & colorMap,
  int kmer, int * globalCount, vector < int > & linkBackState,
  std::unordered_map < int, StateClass > & feasibleStates, std::unordered_set < int > & isolate_comp,
  std::unordered_map < int, vector < int >> & colorTerminal, size_t numComp) {

  std::vector < int > v(4, -1);

  std::vector < std::vector < int >> g(words.size() * kmer, v);

  int numWords = words.size();
  int states = 1;

  for (int i = 0; i < words.size(); ++i) {
    const string & keyword = words[i];

    std::unordered_map < string, int > ::iterator nodeID_iter;
    nodeID_iter = vertexMap.find(keyword);
    int currentNodeID = nodeID_iter -> second;

    int currentColor = 0;
    if (!colorMap.empty()) {
      std::unordered_map < int, int > ::iterator color_iter;
      color_iter = colorMap.find(currentNodeID);
      currentColor = color_iter -> second;
    }

    std::unordered_set < int > ::iterator iter_iso = isolate_comp.find(currentNodeID);
    int isolateNode = 0;
    if (iter_iso != isolate_comp.end()) {
      isolateNode = 1;
    }

    L[0].insert(currentNodeID);

    vector < int > statesForCurtNode;
    int currentState = 0;
    for (int j = 0; j < keyword.size(); ++j) {

      int c = -1;
      if (keyword[j] == 'A') {
        c = 0;
      } else if (keyword[j] == 'C') {
        c = 1;
      } else if (keyword[j] == 'G') {
        c = 2;
      } else if (keyword[j] == 'T') {
        c = 3;
      }

      if (g[currentState][c] == -1) {
        g[currentState][c] = states++;
        if (numComp > 1) {

          linkBackState[g[currentState][c]] = currentState;
        }
      }
      currentState = g[currentState][c];

      L[currentState].insert(currentNodeID);

      statesForCurtNode.push_back(currentState);
    }

    if (numComp > 1) {

      if (degreeNode[currentNodeID] < 0 || isolateNode == 1) {

        feasibleStates[0].insertHC(currentColor, currentState);
        globalCount[currentColor]++;

        std::unordered_map < int, vector < int >> ::iterator colTer_iter;
        colTer_iter = colorTerminal.find(currentColor);
        if (colTer_iter != colorTerminal.end()) {
          colorTerminal[currentColor].push_back(currentState);
        } else {
          vector < int > tempTerminal;
          tempTerminal.push_back(currentState);
          colorTerminal.insert({
            currentColor,
            tempTerminal
          });
        }

        for (auto & it: statesForCurtNode) {
          std::unordered_map < int, StateClass > ::iterator feasible_iter;
          feasible_iter = feasibleStates.find(it);

          if (feasible_iter == feasibleStates.end()) {
            StateClass temp_state;
            temp_state.insertHC(currentColor, currentState);
            feasibleStates.insert({
              it,
              temp_state
            });
          } else {
            feasibleStates[it].insertHC(currentColor, currentState);
          }

        }

      }
    }

  }

  totalStates = states;

  L.resize(totalStates);

  for (int c = 0; c < 4; ++c) {
    if (g[0][c] == -1) {
      g[0][c] = 0;
    }
  }

  vector < int > temp;
  temp.push_back(0);
  stateLevel.push_back(temp);
  temp.clear();

  queue < int > q;
  for (int c = 0; c < 4; ++c) {

    if (g[0][c] != -1 and g[0][c] != 0) {
      f[g[0][c]] = 0;
      q.push(g[0][c]);
      temp.push_back(g[0][c]);
    }
  }

  stateLevel.push_back(temp);

  int index = 1;
  int lastState = stateLevel[1][q.size() - 1];
  temp.clear();

  while (q.size()) {
    int state = q.front();
    q.pop();
    for (int c = 0; c < 4; ++c) {
      if (g[state][c] != -1) {
        int failure = f[state];
        while (g[failure][c] == -1) {
          failure = f[failure];
        }
        failure = g[failure][c];
        f[g[state][c]] = failure;
        q.push(g[state][c]);
        temp.push_back(g[state][c]);
      }
    }

    if (state == lastState && temp.size() != 0) {
      index++;
      stateLevel.push_back(temp);
      lastState = stateLevel[index][temp.size() - 1];
      temp.clear();
    }
  }

  for (auto & it: stateLevel[stateLevel.size() - 1]) {
    terminal.insert(it);
  }

  stateLevel.pop_back();

  return states;
}

void createHE(std::unordered_set < int > & terminal, vector < int > & f,
  std::unordered_map < int, int > & colorMap,
  std::unordered_map < int, vector < int >> & outgoingEdges, int * degreeNode, vector < std::unordered_set < int >> & L,
  std::unordered_map < int, StateClass > & feasibleStates, std::unordered_set < int > & isolate_comp,
  int * globalCount) {

  for (auto & it: terminal) {

    int terminalNodeID = 0;
    auto in_iter = L[it].begin();
    terminalNodeID = * in_iter;

    std::unordered_set < int > ::iterator iter_iso = isolate_comp.find(terminalNodeID);
    if (degreeNode[terminalNodeID] > 0 || iter_iso != isolate_comp.end()) {

      int currentColor = -1;
      std::unordered_map < int, int > ::iterator color_iter;
      color_iter = colorMap.find(terminalNodeID);
      if (color_iter != colorMap.end()) {
        currentColor = color_iter -> second;
      }

      int currentState = it;
      vector < int > outEdge;
      while (currentState != 0) {
        int gotoState = f[currentState];
        outEdge.push_back(gotoState);

        feasibleStates[gotoState].insertHE(currentColor, it);
        globalCount[currentColor]++;
        currentState = gotoState;

      }
      outgoingEdges.insert({
        it,
        outEdge
      });
    }

  }
}

void pruning(int state, std::unordered_map < int, StateClass > & feasibleStates) {

  if (feasibleStates[state].HC.size() == 0 || feasibleStates[state].HE.size() == 0) {

    feasibleStates.erase(state);
    return;
  }

  if (feasibleStates[state].HC.size() == 1) {

    int uniqueColor = -1;
    auto in_iter = feasibleStates[state].HC.begin();
    uniqueColor = in_iter -> first;

    if (feasibleStates[state].eraseColorHE(uniqueColor)) {
      feasibleStates.erase(state);
    }
  } else if (feasibleStates[state].HE.size() == 1) {
    int uniqueColor = -1;
    auto in_iter = feasibleStates[state].HE.begin();
    uniqueColor = in_iter -> first;

    if (feasibleStates[state].eraseColorHC(uniqueColor)) {
      feasibleStates.erase(state);
    }

  }
}

bool pruning_bool(int state, std::unordered_map < int, StateClass > & feasibleStates) {

  if (feasibleStates[state].HC.size() == 0 || feasibleStates[state].HE.size() == 0) {
    return true;
  }

  if (feasibleStates[state].HC.size() == 1) {

    int uniqueColor = -1;
    auto in_iter = feasibleStates[state].HC.begin();
    uniqueColor = in_iter -> first;

    if (feasibleStates[state].eraseColorHE(uniqueColor)) {
      return true;
    }
  } else if (feasibleStates[state].HE.size() == 1) {
    int uniqueColor = -1;
    auto in_iter = feasibleStates[state].HE.begin();
    uniqueColor = in_iter -> first;

    if (feasibleStates[state].eraseColorHC(uniqueColor)) {
      return true;
    }

  }
  return false;
}

void pruneAll(std::unordered_map < int, StateClass > & feasibleStates) {

  auto itr = feasibleStates.cbegin();
  while (itr != feasibleStates.cend()) {
    if (pruning_bool(itr -> first, feasibleStates)) {
      itr = feasibleStates.erase(itr);
    } else {
      ++itr;
    }
  }
}

void updateColorForHC(int terminalState, std::unordered_map < int, StateClass > & feasibleStates, vector < int > & linkBackState,
  std::unordered_set < int > & visitedState, int alpha, int beta) {

  int currentState = terminalState;

  while (currentState != 0) {
    int preState = linkBackState[currentState];

    currentState = preState;

    std::unordered_set < int > ::iterator iter_visit;
    iter_visit = visitedState.find(preState);

    std::unordered_map < int, StateClass > ::iterator feas_iter = feasibleStates.find(preState);
    if (iter_visit == visitedState.end() && feas_iter != feasibleStates.end()) {

      visitedState.insert(preState);

      feasibleStates[preState].updateColorHC(alpha, beta);

      pruning(preState, feasibleStates);
    }

  }
}

void updateHEColor(int terminal, std::unordered_map < int, vector < int >> & outgoingEdges,
  int alpha, int beta, std::unordered_map < int, StateClass > & feasibleStates) {

  std::unordered_map < int, vector < int >> ::iterator out_iter;
  out_iter = outgoingEdges.find(terminal);

  if (out_iter != outgoingEdges.end()) {

    for (auto & nonTerminal: out_iter -> second) {

      std::unordered_map < int, StateClass > ::iterator feas_iter;
      feas_iter = feasibleStates.find(nonTerminal);
      if (feas_iter != feasibleStates.end()) {

        feasibleStates[nonTerminal].updateColorHE(alpha, beta);

        pruning(nonTerminal, feasibleStates);
      }

    }
  }
}

void updateColor(int alpha, int beta, int * globalCount, vector < int > & linkBackState,
  std::unordered_map < int, vector < int >> & outgoingEdges, std::unordered_map < int, StateClass > & feasibleStates,
  std::unordered_map < int, vector < int >> & colorTerminal) {

  globalCount[beta] += globalCount[alpha];
  globalCount[alpha] = -1;

  std::unordered_map < int, vector < int >> ::iterator terCol_iter;
  terCol_iter = colorTerminal.find(alpha);

  std::unordered_set < int > visitedState;

  if (terCol_iter != colorTerminal.end()) {
    for (auto & terminal: terCol_iter -> second) {

      updateColorForHC(terminal, feasibleStates, linkBackState, visitedState, alpha, beta);

      updateHEColor(terminal, outgoingEdges, alpha, beta, feasibleStates);
    }
  }

  for (auto & alpha_elem: colorTerminal[alpha]) {
    colorTerminal[beta].push_back(alpha_elem);
  }
  colorTerminal.erase(alpha);

}

bool selectBackwardEdge(int state, pair < int, int > & pairEdge,
  pair < int, int > & pairColor, std::unordered_map < int, StateClass > & feasibleStates) {

  if (feasibleStates[state].HC.size() == 0 || feasibleStates[state].HE.size() == 0) {
    feasibleStates.erase(state);
    return false;
  }

  int colorAlpha = -1;
  int colorBeta = -1;

  int state_out = -1;
  int state_in = -1;

  auto hc_iter = feasibleStates[state].HC.begin();
  colorAlpha = hc_iter -> first;

  auto he_iter = feasibleStates[state].HE.begin();
  colorBeta = he_iter -> first;

  while (colorAlpha == colorBeta || colorBeta == -1 || colorAlpha == -1) {
    if (he_iter != feasibleStates[state].HE.end()) {
      he_iter++;
      colorBeta = he_iter -> first;
      break;
    } else if (hc_iter != feasibleStates[state].HC.end()) {
      hc_iter++;
      colorAlpha = hc_iter -> first;
      break;
    }
  }

  auto in_iter = feasibleStates[state].HC[colorAlpha].begin();
  state_in = * in_iter;

  auto out_iter = feasibleStates[state].HE[colorBeta].begin();
  state_out = * out_iter;

  pairEdge = make_pair(state_out, state_in);
  pairColor = make_pair(colorBeta, colorAlpha);

  return true;
}

void updateHESet(int terminal, std::unordered_map < int, vector < int >> & outgoingEdges,
  int alpha, std::unordered_map < int, StateClass > & feasibleStates, int * globalCount) {

  std::unordered_map < int, vector < int >> ::iterator out_iter;
  out_iter = outgoingEdges.find(terminal);
  if (out_iter != outgoingEdges.end()) {
    for (auto & nonTerminal: out_iter -> second) {

      std::unordered_map < int, StateClass > ::iterator feas_iter;
      feas_iter = feasibleStates.find(nonTerminal);
      if (feas_iter != feasibleStates.end()) {
        globalCount[alpha]--;
        bool erase_flag = feasibleStates[nonTerminal].deleteElemntHE(alpha, terminal);
        if (erase_flag) {
          feasibleStates.erase(nonTerminal);
        } else {
          pruning(nonTerminal, feasibleStates);

        }
      }
    }
  }

}

void updateHCSet(int terminal, std::unordered_map < int, StateClass > & feasibleStates, vector < int > & linkBackState, int beta, int * globalCount) {

  int currentState = terminal;

  while (currentState != -1) {

    int preState = linkBackState[currentState];
    currentState = preState;

    std::unordered_map < int, StateClass > ::iterator feas_iter;
    feas_iter = feasibleStates.find(preState);

    if (feas_iter != feasibleStates.end()) {
      globalCount[beta]--;

      bool erase_flag = feasibleStates[preState].deleteElemntHC(beta, terminal);
      if (erase_flag) {
        feasibleStates.erase(preState);
      } else {
        pruning(preState, feasibleStates);

      }
    }
  }
}

void algorithm4(vector < vector < int >> & stateLevel, size_t totalStates, int * degreeNode,
  vector < std::unordered_set < int >> L, size_t numComp, int * globalCount, vector < int > & linkBackState,
  std::unordered_map < int, vector < int >> & outgoingEdges, size_t & cost, vector < pair < int, int >> & P,
  std::unordered_map < int, StateClass > & feasibleStates, int kmer, std::unordered_map < int, vector < int >> & colorTerminal) {

  int sizeOfLevel = stateLevel.size();
  for (int i = sizeOfLevel - 1; i >= 0; i--) {

    for (auto & s: stateLevel[i]) {

      std::unordered_map < int, StateClass > ::iterator feas_iter;
      feas_iter = feasibleStates.find(s);

      if (feas_iter != feasibleStates.end()) {

        pair < int, int > pairEdge;
        pair < int, int > pairColor;
        while (selectBackwardEdge(s, pairEdge, pairColor, feasibleStates)) {

          int node1 = -1;
          int node2 = -1;

          auto iter_node1 = L[pairEdge.first].begin();
          node1 = * iter_node1;
          auto iter_node2 = L[pairEdge.second].begin();
          node2 = * iter_node2;

          cost += kmer - i;

          degreeNode[node1]--;
          degreeNode[node2]++;

          int alpha = pairColor.first;
          int beta = pairColor.second;

          if (degreeNode[node1] <= 0) {
            updateHESet(pairEdge.first, outgoingEdges, alpha, feasibleStates, globalCount);
          }
          if (degreeNode[node2] >= 0) {
            updateHCSet(pairEdge.second, feasibleStates, linkBackState, beta, globalCount);
          }

          P.push_back(make_pair(node1, node2));

          if (P.size() == numComp - 1) {
            return;
          }

          if (globalCount[alpha] <= globalCount[beta]) {
            updateColor(alpha, beta, globalCount, linkBackState, outgoingEdges, feasibleStates, colorTerminal);
          } else {
            updateColor(beta, alpha, globalCount, linkBackState, outgoingEdges, feasibleStates, colorTerminal);
          }

        }
      }
    }
    if (P.size() == numComp - 1) {
      return;
    }
  }
}

void findimbalanceNode(int * degreeNode, std::unordered_multimap < int, int > & feasibleEdge,
  int & sum, vector < int > & f, vector < string > & I, std::unordered_map < string, int > vertexMap,
  vector < std::unordered_set < int >> & L, std::unordered_set < int > & terminal) {

  std::unordered_map < string, int > ::iterator iter;
  for (auto & it: I) {
    iter = vertexMap.find(it);
    if (iter != vertexMap.end()) {
      int nodeid = iter -> second;
      if (degreeNode[nodeid] < 0) {
        sum = sum - degreeNode[nodeid];
      } else if (degreeNode[nodeid] > 0) {
        sum = sum + degreeNode[nodeid];
      }
    }
  }
  for (auto & it: terminal) {

    int terminalNodeID = 0;
    auto in_iter = L[it].begin();
    terminalNodeID = * in_iter;

    if (degreeNode[terminalNodeID] > 0) {
      feasibleEdge.insert({
        f[it],
        it
      });
    }
  }

}

void balanceNode(vector < std::unordered_set < int >> & L, std::unordered_multimap < int, int > & feasibleEdge,
  int & numExtraEdge, int * degreeNode, int s, size_t & cost, int weight, int sum, vector < int > & f) {

  std::unordered_multimap < int, int > ::const_iterator Values = feasibleEdge.find(s);
  int Number = feasibleEdge.count(s);

  for (int i = 0; i < Number; i++) {
    int terminalState = Values -> second;

    int terminalNodeID = 0;
    auto in_iter = L[terminalState].begin();
    terminalNodeID = * in_iter;

    while (degreeNode[terminalNodeID] != 0 && L[s].size() != 0) {

      auto iter_u = L[s].begin();
      int u = * iter_u;

      if (degreeNode[u] < 0 && terminalNodeID != u) {

        int minOfTwoNode = min(abs(degreeNode[terminalNodeID]), abs(degreeNode[u]));

        int minNumber = min(minOfTwoNode, sum - 1 - numExtraEdge);

        numExtraEdge += minNumber;

        cost += weight * minNumber;

        degreeNode[terminalNodeID] = degreeNode[terminalNodeID] - minNumber;
        degreeNode[u] = degreeNode[u] + minNumber;
      }
      if (degreeNode[u] >= 0) {
        L[s].erase(u);
      }
      if (numExtraEdge == sum - 1) {
        return;
      }
    }
    ++Values;
    if (degreeNode[terminalNodeID] != 0) {
      feasibleEdge.insert({
        f[s],
        terminalState
      });
    }
  }

}

void findEulerianTrail(int * degreeNode, std::unordered_set < int > & terminal, vector < std::unordered_set < int >> & L,
  int numVertex, vector < vector < int >> & stateLevel, size_t & cost, vector < int > & f, vector < string > & I,
  std::unordered_map < string, int > & vertexMap, int kmer) {

  int sum = 0;
  std::unordered_multimap < int, int > feasibleEdge;

  findimbalanceNode(degreeNode, feasibleEdge, sum, f, I, vertexMap, L, terminal);

  sum = sum / 2;
  if (sum == 1)
    return;

  int sizeOfLevel = stateLevel.size();

  int numExtraEdge = 0;

  for (int i = sizeOfLevel - 1; i >= 0; i--) {

    for (auto & s: stateLevel[i]) {

      balanceNode(L, feasibleEdge, numExtraEdge, degreeNode, s, cost, kmer - i, sum, f);
      feasibleEdge.erase(s);

      if (numExtraEdge == sum - 1) {
        return;
      }
    }
  }

}

void cleanL(vector < std::unordered_set < int >> & L, int * degreeNode, std::unordered_set < int > & terminal) {

  int sizeL = L.size();
  std::unordered_set < int > ::iterator set_iter;

  for (int i = 0; i < sizeL; i++) {
    set_iter = terminal.find(i);
    if (set_iter == terminal.end()) {
      for (auto it = L[i].begin(); it != L[i].end();) {
        if (degreeNode[ * it] >= 0) {
          L[i].erase(it++);
        } else {
          ++it;
        }
      }
    }
  }
}

int main(int argc, char * argv[]) {

  vector < edge_data > multiplicity;
  std::unordered_set < std::string > vertexSet;
  size_t numberEdges = 0;
  int kmer = 0;

  readGraph(multiplicity, vertexSet, argv[1], numberEdges, kmer);

  int numVertex = vertexSet.size();

  cout << "Size of Distinct Edges = " << multiplicity.size() << endl;
  cout << "Total Edges = " << numberEdges << endl;

  std::unordered_map < string, int > vertexMap;
  int index = 0;
  for (auto & it: vertexSet) {
    vertexMap.insert(make_pair(it, index));

    index++;
  }

  int * degreeNode = new int[numVertex];
  for (int i = 0; i < numVertex; i++) {
    degreeNode[i] = 0;
  }

  countDegree(multiplicity, vertexMap, degreeNode);

  //----------------------------------------------------------------------------
  cout << "--------- Find #.Components ---------------" << endl;
  size_t numComp = 0;

  vector < vector < string >> comp;

  auto start_comp = high_resolution_clock::now();

  findComponent(multiplicity, vertexSet, numComp, comp);

  auto end_comp = high_resolution_clock::now();
  auto duration_comp = duration_cast < milliseconds > (end_comp - start_comp);
  double time_comp = duration_comp.count();
  cout << "Runtime find all components: " << time_comp << " milliseconds" << endl;

  vector < string > I;
  cout << "------------get the information of graph-------------" << endl;

  std::unordered_set < int > isolate_comp;

  std::unordered_map < int, int > colorMap;

  if (numComp > 1) {
    graphStatisticsNew(vertexMap, comp, I, degreeNode, isolate_comp, colorMap);

  } else {

    graphStatistics1compNew(vertexMap, I, degreeNode);
  }

  comp.clear();

  size_t totalStates = 0;
  vector < std::unordered_set < int >> L;
  L.resize(I.size() * kmer);
  cout << "#. Imbalanced Nodes  = " << I.size() << endl;

  std::unordered_set < int > terminal;
  vector < vector < int >> stateLevel;

  vector < int > f(I.size() * kmer, -1);
  vector < int > linkBackState(I.size() * kmer, -1);

  int * globalCount = new int[numComp];
  for (int i = 0; i < numComp; i++) {
    globalCount[i] = 0;
  }

  std::unordered_map < int, StateClass > feasibleStates;

  StateClass temState;
  feasibleStates.insert({
    0,
    temState
  });

  std::unordered_map < int, vector < int >> colorTerminal;

  auto start_ac = high_resolution_clock::now();

  buildMatchingMachine(I, totalStates, L, terminal, stateLevel, vertexMap, degreeNode, f,
    colorMap, kmer, globalCount, linkBackState, feasibleStates, isolate_comp, colorTerminal, numComp);

  f.resize(totalStates);
  linkBackState.resize(totalStates);
  L.resize(totalStates);

  auto end_ac = high_resolution_clock::now();
  auto duration_ac = duration_cast < milliseconds > (end_ac - start_ac);
  double time_ac = duration_ac.count();
  cout << "Runtime for create AC Machine : " << time_ac << " milliseconds" << endl;

  size_t cost = 0;
  double time_phase1 = 0;
  size_t cost_rda = 0;

  if (numComp > 1) {

    vector < pair < int, int >> P;

    std::unordered_map < int, vector < int >> outgoingEdges;

    createHE(terminal, f, colorMap, outgoingEdges, degreeNode, L, feasibleStates, isolate_comp, globalCount);

    pruneAll(feasibleStates);

    auto start_phase1 = high_resolution_clock::now();

    algorithm4(stateLevel, totalStates, degreeNode, L, numComp, globalCount, linkBackState, outgoingEdges, cost, P, feasibleStates, kmer, colorTerminal);

    delete[] globalCount;

    auto end_phase1 = high_resolution_clock::now();
    auto duration_phase1 = duration_cast < milliseconds > (end_phase1 - start_phase1);
    time_phase1 = duration_phase1.count();
    cout << "Runtime for Phase 1 : " << time_phase1 + time_comp << " milliseconds" << endl;

    cost_rda = cost;
    cout << "Cost for Component Connection :  " << cost << endl;

  }

  cleanL(L, degreeNode, terminal);

  auto start_phase2 = high_resolution_clock::now();

  findEulerianTrail(degreeNode, terminal, L, numVertex, stateLevel, cost, f, I, vertexMap, kmer);

  auto end_phase2 = high_resolution_clock::now();
  auto duration_phase2 = duration_cast < milliseconds > (end_phase2 - start_phase2);
  double time_phase2 = duration_phase2.count();
  cout << "Runtime Phase 2: " << time_phase2 << " milliseconds" << endl;

  cout << "Runtime for all: " << (time_ac + time_phase1 + time_comp + time_phase2) << " milliseconds" << endl;
  delete[] degreeNode;

  cout << "Extension Cost  = " << cost << endl;

  return 0;
}