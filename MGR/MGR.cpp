#include <boost/graph/adjacency_list.hpp>

#include <boost/graph/connected_components.hpp>

#include <boost/config.hpp>

#include <iostream>

#include <vector>

#include <algorithm>

#include <utility>


#include <fstream>

#include <cstdlib>

#include <cstdio>

#include <cstring>

#include <set>

#include <string>

#include <map>


#include <time.h>

#include <cmath>

#include <chrono>

#define OMP 1

#define RESULT 1

using namespace std;

using namespace boost;
using namespace chrono;

//find component
typedef property < vertex_name_t, std::string > VertexProperty;
typedef adjacency_list < vecS, vecS, undirectedS, VertexProperty > vector_graph_t;

double start, total;

void readFile(map < string, int > & multiplicity, set < string > & vertexSet, char * filename,
  size_t & numberEdges) {
  ifstream file(filename);

  string data;

  while (getline(file, data)) {
    string v1, v2;
    int multi;

    istringstream buffer(data);
    buffer >> v1 >> v2 >> multi;

    string edge = "";

    if (v1.compare(v2) == 0) {
      edge = v1;
    } else {
      edge = v1 + v2.substr(v2.length() - 1, 1);
    }

    multiplicity.insert({
      edge,
      multi
    });
    vertexSet.insert(edge);
    numberEdges += multi;
    buffer.clear();
  }
  file.close();
}

void updateOP(int flag, map < int, vector < pair < int, int >>> & OP, vector < vector < int >> & w,
  int node1, int node2, map < int, pair < int, int >> & IdMap, int numVertex,
  map < int, int > & Q) {

  map < int, int > ::iterator iter_Q;

  for (int i = 0; i < flag; i++) {
    iter_Q = Q.find(i);

    if (iter_Q != Q.end()) {
      int preNode = i;
      if (i >= numVertex) {
        map < int, pair < int, int >> ::iterator iter_map;

        iter_map = IdMap.find(i);
        preNode = iter_map -> second.second;
      }
      if (preNode != node1) {
        map < int, vector < pair < int, int >>> ::iterator iter_op;
        if (w[preNode][node1] != 0) {
          iter_op = OP.find(w[preNode][node1]);

          if (iter_op == OP.end()) {
            vector < pair < int, int >> temp;
            temp.push_back(make_pair(i, flag));
            OP.insert(make_pair(w[preNode][node1], temp));
          } else {
            iter_op -> second.push_back(make_pair(i, flag));
          }
        }

      }
    }

  }

  for (int i = 0; i < flag; i++) {
    iter_Q = Q.find(i);
    if (iter_Q != Q.end()) {
      int preNode = i;
      if (i >= numVertex) {
        map < int, pair < int, int >> ::iterator iter_map;

        iter_map = IdMap.find(i);
        preNode = iter_map -> second.first;
      }
      if (preNode != node2) {
        map < int, vector < pair < int, int >>> ::iterator iter_op;

        if (w[node2][preNode] != 0) {
          iter_op = OP.find(w[node2][preNode]);
          if (iter_op == OP.end()) {
            vector < pair < int, int >> temp;
            temp.push_back(make_pair(flag, i));
            OP.insert(make_pair(w[node2][preNode], temp));
          } else {
            iter_op -> second.push_back(make_pair(flag, i));
          }
        }

      }
    }
  }
}

void getNewId(int & node1, int & node2, int numVertex, map < int, pair < int, int >> & IdMap,
  int & addNewPair, int & newPairId) {

  map < int, pair < int, int >> ::iterator iter_map;
  if (node1 >= numVertex) {
    iter_map = IdMap.find(node1);
    if (iter_map != IdMap.end()) {
      node1 = iter_map -> second.first;
      if (node2 == iter_map -> second.second) {
        addNewPair = 0;
        newPairId = iter_map -> first;
      }
    }
  }
  if (node2 >= numVertex) {
    iter_map = IdMap.find(node2);
    if (iter_map != IdMap.end()) {
      node2 = iter_map -> second.second;
      if (node1 == iter_map -> second.first) {
        addNewPair = 0;
        newPairId = iter_map -> first;
      }
    }
  }

  pair < int, int > temp = make_pair(node1, node2);

  for (auto & it: IdMap) {
    if (it.second == temp) {
      newPairId = it.first;
      addNewPair = 0;
      break;
    }
  }

}

void greedy(map < int, int > & Q, map < int, vector < pair < int, int >>> & OP, vector < vector < int >> & w,
  int numVertex, size_t kmer, int & cost) {

  cout << "---------------Greedy--------------------" << endl;
  int flag = numVertex;

  map < int, pair < int, int >> IdMap;

  map < int, vector < pair < int, int >>> ::reverse_iterator rit;
  map < int, int > ::iterator iter_Q1;
  map < int, int > ::iterator iter_Q2;
  map < int, int > ::iterator iter_Q3;
  map < int, int > ::iterator iter_new;

  while (Q.size() > 1) {

    if (OP.empty()) {
      int sizeQ = 0;
      for (auto it: Q) {
        sizeQ += it.second;

      }
      cost += (sizeQ - 1) * (kmer - 1);
      break;
    } else {
      rit = OP.rbegin();
      int node1 = rit -> second.back().first;
      int node2 = rit -> second.back().second;

      iter_Q1 = Q.find(node1);
      iter_Q2 = Q.find(node2);

      if (iter_Q1 != Q.end() && iter_Q2 != Q.end()) {
        iter_Q1 -> second--;
        iter_Q2 -> second--;
        if (iter_Q1 -> second == 0) {
          Q.erase(iter_Q1);
        }
        if (iter_Q2 -> second == 0) {
          Q.erase(iter_Q2);
        }
        int newPairId = flag;

        map < int, pair < int, int >> ::iterator iter_map;

        if (node1 >= numVertex) {
          iter_map = IdMap.find(node1);
          if (iter_map != IdMap.end()) {
            node1 = iter_map -> second.first;
          }
        }
        if (node2 >= numVertex) {
          iter_map = IdMap.find(node2);
          if (iter_map != IdMap.end()) {
            node2 = iter_map -> second.second;
          }
        }

        cost += kmer - rit -> first - 1;
        Q.insert(make_pair(newPairId, 1));
        IdMap.insert(make_pair(newPairId, make_pair(node1, node2)));

        updateOP(flag, OP, w, node1, node2, IdMap, numVertex, Q);
        flag += 1;
      } else {

        rit -> second.pop_back();
        if (rit -> second.size() == 0) {
          OP.erase(prev(OP.end()));
        }

        continue;
      }

    }

  }

  cout << "Final Cost  = " << cost << endl;
}

void readMatrix(char * filename, vector < vector < int >> & ov, size_t numVertex) {

  ifstream myfile;
  myfile.open(filename);

  for (size_t i = 0; i < numVertex; i++) {
    for (size_t j = 0; j < numVertex; j++) {
      myfile >> ov[i][j];
    }
  }
}

int main(int argc, char * argv[]) {

  map < string, int > multiplicity;

  set < string > vertexSet;
  size_t kmer = atoi(argv[3]);
  size_t numberEdges = 0;
  readFile(multiplicity, vertexSet, argv[1], numberEdges);

  cout << "Size of Distinct Edges = " << multiplicity.size() << endl;

  size_t numVertex = vertexSet.size();
  int total_length = 0;
  int count = 0;
  map < string, int > vertexMap;
  cout << "Number of  Vertex = " << numVertex << endl;

  for (auto & it: vertexSet) {
    vertexMap.insert(make_pair(it, count));
    count++;
  }

  vector < vector < int >> w(numVertex, vector < int > (numVertex, 0));

  readMatrix(argv[2], w, numVertex);

  map < int, int > Q;
  map < string, int > ::iterator iter;

  for (auto & it: multiplicity) {
    total_length += it.first.length();
    iter = vertexMap.find(it.first);
    if (iter != vertexMap.end()) {
      Q.insert(make_pair(iter -> second, it.second));
    }

  }

  map < int, vector < pair < int, int >>> OP;
  map < int, vector < pair < int, int >>> ::iterator iter_op;

  for (size_t i = 0; i < numVertex; ++i) {
    for (size_t j = 0; j < numVertex; ++j) {
      if (w[i][j] == 0) {
        continue;
      } else {
        iter_op = OP.find(w[i][j]);
        if (iter_op == OP.end()) {
          vector < pair < int, int >> temp;
          temp.push_back(make_pair(i, j));
          OP.insert(make_pair(w[i][j], temp));
        } else {
          iter_op -> second.push_back(make_pair(i, j));
        }
      }

    }

  }

  int cost = 0;

  //--------------------------------------------------------------------------------------
  auto greedy_start = high_resolution_clock::now();
  greedy(Q, OP, w, numVertex, kmer + 1, cost);

  w.clear();
  auto greedy_end = high_resolution_clock::now();
  auto duration = duration_cast < milliseconds > (greedy_end - greedy_start);

  cout << endl << "Time taken by Greedy: " << duration.count() << " miilliseconds" << endl << endl;

  return 0;

}